UNICAMENTE SE PUEDEN SUBIR ARCHIVOS .KML 
A ESTE REPOSITORIO, LOS ARCHIVOS CON EXTENSION
.KMZ NO SERVIRIAN PARA LA APLICACION.

LOS ARCHIVOS KMZ AL DESCOMPRIMIRLOS CON WINRAR
QUEDAN CON EXTENSION .KML   .A ESTE ARCHIVO 
HAY QUE CAMBIARLE EL NOMBRE POR EL RESPECTIVO COD
DEL VENDEDOR AL CUAL SE LE QUIERE ASIGNAR SU ZONA
O POLIGONO.

POR EJEMPLO SI SE LE VA ASIGNAR UN .KML A VD100 EL ARCHIVO EN EL REPOSITORIO DEBERIA NOMBRARSE VD100.KML